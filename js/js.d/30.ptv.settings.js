/**
	@brief		Handles the storage of settings in the user's browser.
	@since		2016-11-14 17:57:24
**/
function ptv_settings( name )
{
	$$ = this;

	/**
		@brief		Automatically save new settings in the storage.
		@details	Else call save() manually.
		@since		2016-11-14 18:48:23
	**/
	$$.autosave = true;

	/**
		@brief		The key / name under which to store the settings in the storage.
		@since		2016-11-14 18:46:45
	**/
	$$.name = name;
	/**
		@brief		Where the settings are stored.
		@since		2016-11-14 18:47:17
	**/
	$$.settings = false;

	/**
		@brief		Clear a key.
		@since		2017-01-04 16:50:18
	**/
	$$.clear = function( key )
	{
		delete $$.settings[ key ];
		if ( $$.autosave )
			$$.save();
	}

	/**
		@brief		Try to return a key in the settings.
		@since		2016-11-14 18:47:51
	**/
	$$.get = function( key, default_value )
	{
		if ( $$.settings[ key ] === undefined )
			return default_value;
		return $$.settings[ key ];
	}

	/**
		@brief		Load the setings from storage.
		@since		2016-11-14 18:52:33
	**/
	$$.load = function()
	{
		try
		{
			if ( typeof( localStorage ) == 'undefined' )
				throw "No Localstorage";
			$$.settings = localStorage.getItem( $$.name );
			if ( ! $$.settings )
				throw 'null';
			$$.settings = JSON.parse( $$.settings );
		}
		catch ( e )
		{
			$$.settings = {};
		}
	}

	/**
		@brief		Save the settings into the storage.
		@since		2016-11-14 18:48:41
	**/
	$$.save = function()
	{
		if ( typeof( localStorage ) == 'undefined' )
			return;
		var settings = JSON.stringify( $$.settings );
		localStorage.setItem( $$.name, settings );
	}

	/**
		@brief		Set a value in the settings.
		@since		2016-11-14 18:48:06
	**/
	$$.set = function( key, value )
	{
		$$.settings[ key ] = value;
		if ( $$.autosave )
			$$.save();
	}

	$$.load();

	return this;
};
