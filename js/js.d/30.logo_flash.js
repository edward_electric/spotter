/**
	@brief		Flash the PTV logo on the screen.
	@details	Used at startup.
	@since		2017-01-22 21:56:22
**/
function logo_flash()
{
	var $body = $( 'body' );
	var $logo = $( '<div class="logo_flash logo"></div>' )
		.appendTo( $body );
	var $as_seen_on_tv = $( '<div class="as_seen_on_tv"></div>' )
		.appendTo( $logo );
	setTimeout( function()
	{
		$logo.fadeOut( 2000 );
	}, 2000 );
};
