/**
	@brief		Closes and expands help sections of text.
	@since		2017-01-18 13:50:24
**/
function help_sections( options )
{
	$.each( $( '.help_section' ), function( index, item )
	{
		$item = $( item );
		$( '<div class="toggle"><span class="icon">?</span></div>' ).prependTo( $item );
		$item.click( function()
		{
			if ( $item.hasClass( 'closed' ) )
				$item.removeClass( 'closed' ).addClass( 'opened' );
			else
				$item.addClass( 'closed' ).removeClass( 'opened' );
			$( 'p', $item ).toggle();
		} ).click();
	} );
};
