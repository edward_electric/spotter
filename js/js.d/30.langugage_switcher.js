/**
	@brief		Handle the language switcher UI thing, if available.
	@since		2017-01-20 01:55:30
**/
function language_switcher()
{
	var $language_switcher = $( '#language_switcher' );
	if ( $language_switcher.length < 1 )
		return;
	$language_switcher.change( function()
	{
		var $selected = $( ":selected", $language_switcher );
		document.location = $selected.data( 'url' );
	} );
};
