/**
	@brief		PTV tabs.
	@since		2016-11-09 20:36:15
**/
jQuery(function($)
{
	$.fn.extend(
	{
		ptv_tabs : function()
		{
			return this.each(function ()
			{
				var $$ = $(this);

				var $tabs = $( '.tab_selector', $$ );

				$$.init_tabs = function()
				{
					$tabs.click( function()
					{
						var $this = $( this );
						$tabs.removeClass( 'selected' );
						$this.addClass( 'selected' );

						// Hide all tabs and show only the one selected.
						$( '.tab_content', $$ ).hide();
						var id = $this.data( 'content_id' );
						var selector = '#' + id + '.tab_content';
						$( selector ).show();
					} );
				}

				$$.init_tabs();

				// Select the first tab.
				$tabs.first().click();
			});
		}
	});
}(jQuery));
