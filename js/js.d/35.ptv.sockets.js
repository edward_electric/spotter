socket_namespace = '';

var ptv_socket;

function ptv_socket_connect( socket_namespace )
{
	if ( socket_namespace === undefined )
		socket_namespace = '';
	var url = '';
	url += ( 'https:' == document.location.protocol ? 'https' : 'http' );
	url += '://' + document.domain + ':' + location.port + socket_namespace;
    ptv_socket = io.connect( url );

	$( window ).on( 'beforeunload', function()
	{
		ptv_socket.disconnect();
	});

	return ptv_socket;
}
