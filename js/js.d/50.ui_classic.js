jQuery(function($)
{
	$.fn.extend(
	{
		ui_classic : function()
		{
			return this.each(function ()
			{
				var $$ = $(this);

				$$.window = $( window );

				// Move us out of the way.
				$$.appendTo( $( 'body' ) );

				$$.browse = {};
				$$.browse.blink_mode = true;

				$$.client_id = Math.random() * 1000000 + 1000000		// Our unique client ID.
				$$.compare_time = 5;									// How many seconds to compare previous images when audio snapshotting?
				$$.current_image = null;

				$$.debug = {};
				$$.debug.active = false;								// Is debugging active?
				$$.debug.internet_access_check = {};				// Check for internet access.
				$$.debug.internet_access_check.interval = false;	// The interval used to check for internet access.

				$$.imagebuffer = double_buffer( $$ );
				$$.images = linked_list();
				/**
					@brief		An array of image_id : description.
					@since		2018-03-04 23:47:31
				**/
				$$.image_descriptions = [];
				$$.$image_description = false;
				$$.$image_description_modal = false;

				/**
					@brief		All buttons and other inputs.
					@since		2016-11-14 19:23:19
				**/
				$$.inputs = {};

				$$.live = {};									// Settings for live mode.
				$$.live.$input;									// The checkbox that controls live mode.
				$$.live.current_index = 0;						// The index of the current live image.
				$$.live.max_index = 0;							// The index of the lastest available live image.
				$$.live.switch_timeout = 0;						// The timeout after which it switched back to live mode.

				$$.mode = '';

				/**
					@brief		Stores signal meter stuff.
					@since		2017-02-06 17:11:39
				**/
				$$.signal_meter = {};
				$$.signal_meter.$div;								// The jquery div of the SM.
				$$.signal_meter.$value;								// Div of the value.
				$$.signal_meter.activate_time = 10000;				// Wait x seconds before activating the meter.
				$$.signal_meter.backlog = 0;						// Set this to > 0 to keep the ping alive.
				$$.signal_meter.display_timeout = false;			// Timeout for displaying the value / reping.
				$$.signal_meter.latest_image = 0;					// When an image was last received.
				$$.signal_meter.new_image_timeout = false;			// Hard limit for waiting for new images.
				$$.signal_meter.pinging = false;					// Is there a ping in the works?

				$$.api = source_api( $$ );
				$$.source_id = $$.data( 'source_id' );

				$$.storage = ptv_settings( 'ui_classic' );

				// ---------
				// FUNCTIONS
				// ---------

				/**
					@brief		Return the standard data array for emitting.
					@since		2015-09-07 18:41:36
				**/
				$$.emit_data = function()
				{
					return {
						'client_id' : $$.client_id,
						'source_id' : $$.source_id
					};
				}

				/**
					@brief		Request a list of all of the available images, which will be only the keepers.
					@since		2015-08-27 21:15:02
				**/
				$$.get_keepers = function()
				{
					var data = $$.emit_data();
					$$.socket.emit( 'get_keepers', data );
				}

				/**
					@brief		Return the description of an image, if any.
					@since		2018-03-05 00:26:14
				**/
				$$.get_image_description = function( image_id )
				{
					if ( ! image_id )
						image_id = $$.current_image.data;
					if ( $$.image_descriptions[ image_id ] === undefined )
						return '';
					return $$.image_descriptions[ image_id ];
				}

				/**
					@brief		Initialize the javascript itself.
					@since		2015-08-27 21:14:28
				**/
				$$.init = function()
				{
					// DEBUG
					// logo_flash();
					$$.init_signal_meter();
					$$.init_socket();
					$$.init_debug();
					$$.init_audio();
					$$.init_inputs();
					$$.init_image_description_modal();
					$$.init_counters();
					language_switcher();
					$$.init_resize();
					$$.init_rename();
					$$.init_time();
					$$.init_touching();
					help_sections();
					$$.switch_to_live_mode();

					$$.$image_description = $( '.image_description.text' );//.hide();

					// We might put this in an init_menu later.
					$( '.modal_bottom' ).click( function()
					{
						$( '#view_menu' ).foundation( 'reveal', 'close' );
					} );
				};

				/**
					@brief		Enable the audio snapshot button and function.
					@since		2016-08-07 19:34:32
				**/
				$$.init_audio = function()
				{
					$$.audio = ui_audio_processor();

					$$.audio.$audio_snapshot = $( '.audio_snapshot' );

					$$.audio.$buttons = $( '.button', $$.audio.$audio_snapshot );
					$$.audio.$volume = $( '.volume', $$.audio.$audio_snapshot );

					$$.audio.add_callback( 'volume', '1', function( data )
					{
						$( '.audio .max' ).html( data.volume );

						// Only show the volume if in setup mode.
						if ( ! $( '.setup.button', $$.audio.$audio_snapshot ).hasClass( 'selected' ) )
							return;

						var current_volume = $( '.audio_snapshot .volume' ).html();
						var new_volume = Math.round( data.volume * 1000 ) / 1000;		// Round to 2 decimals.
						if ( new_volume > current_volume )
							$( '.audio_snapshot .volume' ).html( new_volume );
					} );

					$$.audio.add_callback( 'bang', '1', function( data )
					{
						// We only listen to bangs in live mode.
						if ( ! $$.mode_is( 'live' ) )
							return;

						if ( $$.inputs[ 'snapshot' ].is_busy() )
							return;
						$$.inputs[ 'snapshot' ].busy();

						// Wait for the next image, which is in the pipe...
						$$.wait_for_new_image( function()
						{
							// And the next image we snapshot!
							$$.wait_for_new_image( function()
							{
								$$.inputs[ 'snapshot' ].not_busy();
								$$.inputs[ 'snapshot' ].click();
							} );
						} );
					} );

					$$.audio.$buttons.click( function()
					{
						$$.audio.$buttons.removeClass( 'selected' );
						$( this ).addClass( 'selected' );
					} );

					$( '.off.button', $$.audio.$audio_snapshot ).click( function()
					{
						$$.audio.set_sensitivity( 0 );
					} )
					.click();	// Always select off when we start.

					$( '.on.button', $$.audio.$audio_snapshot ).click( function()
					{
						var volume = $$.audio.$volume.html();
						// Subtract 10% for the sake of variation.
						volume = volume * 0.9;
						$$.storage.set( 'audio_volume', volume );
						$$.audio.set_sensitivity( volume );
					} );

					$( '.setup.button', $$.audio.$audio_snapshot ).click( function()
					{
						$$.audio.set_sensitivity( 100 );
						$$.audio.$volume.html( '0' );
					} );

					var old_volume = $$.storage.get( 'audio_volume', '000' );
					$$.audio.$volume.html( old_volume );
				};

				/**
					@brief		Init the browse counters.
					@since		2015-08-29 22:12:58
				**/
				$$.init_counters = function()
				{
					$$.current_counter = $( '.counter .current', $$ );
					$$.max_counter = $( '.counter .max', $$ );
				}

				/**
					@brief		Init debugging functions.
					@since		2016-11-03 07:11:58
				**/
				$$.init_debug = function()
				{
					$$.inputs[ 'debug' ] = $( '#debug_input' );

					$$.inputs.debug.change( function()
					{
						$$.debug.active = $$.inputs.debug.prop( 'checked' );
						if ( $$.debug.active )
						{
							$( '.debug' ).show();
							$$.storage.set( 'debug_checked', true );
							$$.signal_meter.fail();		// Calling a fail will start the loop.
						}
						else
						{
							$( '.debug' ).hide();
							$$.storage.set( 'debug_checked', false );
						}
						$$.init_internet_access_check();
					} );

					/**
						@brief		The update PTV link.
						@since		2017-12-03 13:19:41
					**/
					$( '.update_ptv' ).click( function()
					{
						$( '.update_ptv' ).fadeTo( 500, 0.25 );
						$$.socket.emit( 'update_ptv' );
					} );

					var value = $$.storage.get( 'debug_checked', false );
					$$.inputs.debug.prop( 'checked', value );
					$$.inputs.debug.change();
				}

				/**
					@brief		Initialize the image description editor modal.
					@since		2018-03-05 00:17:51
				**/
				$$.init_image_description_modal = function()
				{
					$$.$image_description_modal = $( '#image_description_modal' );
					$( document ).on( 'opened.fndtn.reveal', '[data-reveal]', function ()
					{
						// If we snapshotted, don't switch back to live mode automatically.
						clearTimeout( $$.live.switch_timeout );
						var $modal = $(this);
						if ( $modal.attr( 'id' ) != 'image_description_modal' )
							return;
						// Set the description input.
						var description = $$.get_image_description();
						$( '#image_description_input', $modal )
							.val( description )
							.focus();
					});

					$( '#image_description_input', $$.$image_description_modal ).keyup( function( event )
					{
						var code = event.which;
						 if( code==13 )
						 {
						 	 event.preventDefault();
						 	 $$.save_image_description.click();
						 }
					} );

					$$.save_image_description = $( '.save_image_description.button' );
					$$.save_image_description.click( function()
					{
						// Get the description from the input.
						var description = $( '#image_description_input', $$.$image_description_modal ).val();
						description = description.trim();
						// Send it to the server.
						var image_id = $$.current_image.data;		// Conv.
						var data = $$.emit_data();
						data.index = image_id;
						data.image_description = description;
						$$.socket.emit( 'set_image_description', data );
						$$.$image_description_modal.foundation( 'reveal', 'close' );
						$$.image_descriptions[ image_id ] = description;
						$$.show_image_description();
						$$.get_keepers();
					} );
				}

				/**
					@brief		Initialize the buttons and inputs.
					@since		2015-08-27 21:18:19
				**/
				$$.init_inputs = function()
				{
					$$.inputs[ 'browse' ] = $( '.browse_mode.button', $$ ).click( function()
					{
						if ( $$.images.length < 1 )
							return;

						$$.switch_to_browse_mode();
					} );

					$$.inputs[ 'compare_time' ] = $( '.compare_time input' ).change( function()
					{
						$$.compare_time = $$.inputs[ 'compare_time' ].val();
						// Filter: limit to between 0 and 3600.
						$$.compare_time = Math.min( $$.compare_time, 3600 );
						$$.compare_time = Math.max( $$.compare_time, 0 );
						$$.inputs[ 'compare_time' ].val( $$.compare_time );
						$$.storage.set( 'setting compare_time', $$.compare_time );
					} );
					$( '.compare_time input' ).val( $$.storage.get( 'compare_time', 5 ) ).change();

					$$.inputs[ 'next' ] = $( '.next.button', $$ ).click( function()
					{
						$$.swipe_and_change( $$.current_image.next, -100 );
					} );

					$$.inputs[ 'previous' ] = $( '.previous.button', $$ ).click( function()
					{
						$$.swipe_and_change( $$.current_image.previous, + 100 );
					} );

					$$.inputs[ 'snapshot' ] = $( '.snapshot.button', $$ );

					$$.inputs[ 'trash' ] = $( '.trash.button', $$ ).click( function()
					{
						$$.inputs[ 'trash' ].fadeTo( 250, 0.5 );

						var current_id = $$.current_image.id;
						var current_index = $$.current_image.data

						if ( current_id == 1 )
						{
							$$.inputs[ 'trash' ].fadeTo( 0, 1 );
							$$.switch_to_live_mode();
						}

						// Delete the current image.
						var data = $$.emit_data();
						data.index = current_index;
						$$.socket.emit( 'trash_image', data );

						if ( current_id > 1 )
							$$.wait_for_new_keepers( function()
							{
								$$.inputs[ 'trash' ].fadeTo( 250, 1 );
								$$.show_image( $$.current_image );
								$$.maybe_blink_previous( $$.current_image );
							} );
					} );
					/**
						@brief		Mark the button as busy.
						@since		2016-11-17 17:25:40
					**/
					$$.inputs.snapshot.busy = function()
					{
						$$.inputs[ 'snapshot' ].addClass( 'busy' );
					}

					/**
						@brief		Is this thing busy?
						@since		2016-11-17 17:25:40
					**/
					$$.inputs.snapshot.is_busy = function()
					{
						return $$.inputs[ 'snapshot' ].hasClass( 'busy' );
					}

					/**
						@brief		Unmark the button as busy.
						@since		2016-11-17 17:25:40
					**/
					$$.inputs.snapshot.not_busy = function()
					{
						$$.inputs[ 'snapshot' ].removeClass( 'busy' );
					}

					$$.inputs.snapshot.click( function()
					{
						// Only allow the button to be clicked if we are in live mode.
						if ( ! $$.mode_is( 'live' ) )
							return;
						// If in browse mode, go to live mode.
						if ( $$.mode_is( 'browse' ) )
						{
							$$.switch_to_live_mode();
							return;
						}

						// Don't do anything if it's busy.
						if ( $$.inputs.snapshot.is_busy() )
							return;

						$$.inputs[ 'snapshot' ].busy();

						$$.wait_for_new_image( function()
						{
							// Keeping the old_value is to prevent the latest image from being replaced and therefore preventing blinking.
							$$.take_snapshot();
							$$.wait_for_new_keepers( function()
							{
								$$.inputs[ 'snapshot' ].not_busy();

								// Only compare if the user wants to do so.
								if ( $$.compare_time > 0 )
								{
									$$.switch_to_browse_mode();
									$$.live.switch_timeout = setTimeout( function()
									{
										$$.switch_to_live_mode();
									}, $$.compare_time * 1000 );
								}
							} );
						} );
					} );

					$$.inputs[ 'snapshot_mode' ] = $( '.live_mode.button', $$ ).click( function()
					{
						$$.switch_to_live_mode();
					} );
				}

				/**
					@brief		Check for an internet connection every few seconds.
					@details	Hides / shows the html elements that are affected by internet access.
					@since		2017-12-03 12:36:40
				**/
				$$.init_internet_access_check = function()
				{
					clearInterval( $$.debug.internet_access_check.interval );

					if ( $$.debug.active )
					{
						$$.debug.internet_access_check.interval = setInterval( function()
						{
							$$.socket.emit( 'check_internet_access' );
						}, 5000 );
					}
					else
					{
						$( '.needs_internet_access' ).fadeOut();
					}
				}

				/**
					@brief		Handle the rename dialog
					@since		2016-08-07 16:43:45
				**/
				$$.init_rename = function()
				{
					$$.rename_dialog = {};

					$$.rename_dialog.input = $( '#rename_camera_name' );

					$$.rename_dialog.save_timeout = false;

					// Get the url for the description. Use + to clone the string itself.
					var url = document.location + '';
					$$.rename_dialog.url = url.replace( '/view', '/settings/' );

					/**
						@brief		Just clears the rename timeout.
						@since		2016-11-11 21:48:24
					**/
					$$.rename_dialog.clear_timeout = function()
					{
						clearTimeout( $$.rename_dialog.save_timeout );
						$$.rename_dialog.save_timeout = false;
					}

					/**
						@brief		Sets the text from the value in the API
						@since		2016-11-11 21:53:14
					**/
					$$.rename_dialog.get_name = function()
					{
						$$.api.load( function( data )
						{
							$$.rename_dialog.input.val( data.description );
						} );
					}

					/**
						@brief		Save the input as the user types, but wait a while for him to finish typing.
						@since		2016-11-11 21:43:44
					**/
					$$.rename_dialog.input.change( function()
					{
						$$.rename_dialog.clear_timeout();
						$$.rename_dialog.save_timeout = setTimeout( function()
						{
							$$.rename_dialog.save();
						}, 1000 );
					} );

					/**
						@brief		Blur = save immediately.
						@since		2016-11-11 21:45:05
					**/
					$$.rename_dialog.input.blur( function()
					{
						$$.rename_dialog.clear_timeout();
						$$.rename_dialog.save();
					} );

					/**
						@brief		Send off the new description to PTV.
						@since		2016-11-11 21:43:31
					**/
					$$.rename_dialog.save = function()
					{
						var new_description = $$.rename_dialog.input.val();
						$$.api.store( { 'description' : new_description }, function()
						{
							$$.rename_dialog.get_name();
						});
					}
				}

				/**
					@brief		Initialize the window resizing.
					@since		2015-08-27 21:15:51
				**/
				$$.init_resize = function()
				{
					$$.window.resize( function()
					{
						$$.size_to_fit();
					} );
					$$.window.resize();
				}

				/**
					@brief		Handle the signal meter that shows any signal problems.
					@since		2017-02-06 17:10:53
				**/
				$$.init_signal_meter = function()
				{
					var sm = $$.signal_meter;		// Conv.

					sm.latest_image = new Date().getTime();

					sm.$div = $( '.signal_meter', $$ ).hide();
					sm.$value = $( '.value', sm.$div );

					/**
						@brief		Function to init the meter after failing the timeouts.
						@since		2017-02-06 19:38:50
					**/
					sm.fail = function()
					{
						sm.backlog = 3;
						sm.$div.stop().fadeTo( 500, 1 );
						sm.ping();
					}

					sm.new_image = function()
					{
						sm.set_new_image_timeout();

						var time = new Date().getTime();

						var diff = time - sm.latest_image;
						diff = Math.abs( diff );
						sm.latest_image = time;

						// Has it been long since the last image was received?
						if ( diff < sm.activate_time )
						{
							// Dec by 1 until 0.
							sm.backlog = Math.max( sm.backlog - 1, 0 );
							return;
						}

						// It HAS been a long time. Enable pinging.
						sm.fail();
					}

					sm.ping = function()
					{
						if ( sm.backlog < 1 )
						{
							sm.$value.html( 'OK!' );
							setTimeout( function()
							{
								sm.$div.stop().fadeTo( 2500, 0, function()
								{
									sm.$div.hide();
								} );
							}, 5000 );
							return;
						}

						// Don't double ping.
						if ( sm.pinging )
							return;

						sm.pinging = true;

						var sent_time = new Date().getTime();
						jQuery.ajax( {
							'timeout' : 10000,		// 10 seconds of timeout.
						} )
						.always( function()
						{
							if ( $$.debug.active )
								sm.fail();
						} )
						.done( function( data, text, jqxhr )
						{
							time = new Date().getTime();
							difference = Math.abs( time - sent_time );

							clearTimeout( sm.display_timeout );
							sm.display_timeout = setTimeout( function()
							{
								// Show time spent.
								sm.$value.html( difference + 'ms' );
								sm.pinging = false;
								sm.ping();
							}, 100 );
						} )
						.fail( function()
						{
							clearTimeout( sm.display_timeout );
							sm.display_timeout = setTimeout( function()
							{
								sm.$value.html( '<span class="infin">&infin;</span>' );
								sm.pinging = false;
								sm.ping();
							}, 100 );
						} );
					}

					/**
						@brief		Set or reset the interval that waits for the new image.
						@since		2017-02-06 19:40:11
					**/
					sm.set_new_image_timeout = function()
					{
						clearTimeout( sm.new_image_timeout );
						sm.new_image_timeout = setTimeout( function()
						{
							// If we've gotten this far, then we're tired of waiting for a new image.
							sm.fail();
							sm.set_new_image_timeout();
						}, sm.activate_time );
					}

					sm.set_new_image_timeout();
				}

				/**
					@brief		Initialize the socket handling.
					@since		2015-08-27 21:14:19
				**/
				$$.init_socket = function()
				{
					$$.socket = ptv_socket_connect();

					$$.socket.on( 'connect', function()
					{
						$$.touch_source();
						$$.get_keepers();
					} );

					$$.socket.on( 'get_keepers', function( data )
					{
						if ( data.source_id != $$.source_id )
							return;

						$$.images.flush();
						$$.image_descriptions = [];

						if ( data.images.length < 1 )
							$$.inputs.snapshot_mode.click();
						else
						{
							// The data itself is never used for anything, so we just add the id as the index and data.
							for ( counter = 0; counter < data.images.length; counter++ )
								$$.images.append( counter + 1, data.images[ counter ] );

							// Add the last one separately for the "view last image" function.
							$$.images.append( 'latest', data.images[ counter - 1 ] );
						}

						$$.image_descriptions = data.descriptions;

						if ( $$.mode_is( 'browse' ) )
							var showing_latest = ( $$.current_image.id == 'latest' );

						$$.reload_current_image();
						$$.update_counters();
						$$.show_image_description();

						// If we are blinking the latest
						if ( showing_latest )
						{
							$$.show_image( $$.current_image );
							$$.show_image( $$.current_image.previous.previous );
						}
					} );

					$$.socket.on( 'internet_access', function( data )
					{
						if ( data.status == true )
						{
							$( '.needs_internet_access' ).fadeIn();
						}
						else
							$( '.needs_internet_access' ).fadeOut();
					} );

					$$.socket.on( 'new_image', function( data )
					{
						$$.signal_meter.new_image();
						// We're only interested in our current source.
						if ( data.id != $$.source_id )
							return;

						// Keep track of the latest available image.
						$$.live.max_index = data.index;

						if ( $$.mode_is( 'live' ) )
						{
							// Dupe check.
							if ( $$.current_image != null )
								if ( $$.current_image.id == data.index )
									return;
							// This image is interesting to us.
							$$.live.current_index = data.index;
							$$.show_image( { data : $$.live.current_index } );
						}
					} );

					// PTV has been updated. Reboot the system.
					$$.socket.on( 'ptv_updated', function( data )
					{
						// Visit the reboot link.
						var url = $( '.reboot a' ).prop( 'href' );
						window.location = url;
					} );

					$$.socket.emit( 'connect' );
				} // $$.init_socket = function()

				/**
					@brief		Send the time to the rasp.
					@since		2017-01-03 15:51:25
				**/
				$$.init_time = function()
				{
					var d = new Date();
					var string = d + '';
					// Strip off the GMT.
					string = string.replace( /\ GMT.*/, '' );
					// Strip off the day name.
					string = string.substr( 4 );
					$$.api.store( { 'time' : string } );
				}

				/**
					@brief		Initialize the touching.
					@since		2016-11-04 18:41:55
				**/
				$$.init_touching = function()
				{
					$$.touch_manipulation = touch_manipulation( {
						'element' : $( '.image.layer', $$ ),
						'root' : $$[0 ],		// We need a root that is not <body> otherwise the menu won't scroll down.
					} );

					// Add swiping.
					$$.touch_manipulation.hammertime.on( 'swipeleft', function()
					{
						if ( ! $$.mode_is( 'browse' ) )
							return;

						// Only change if the image is unzoomed.
						if ( $$.touch_manipulation.zoom.value != 100 )
							return;

						$$.inputs[ 'next' ].click();
					} );
					$$.touch_manipulation.hammertime.on( 'swiperight', function()
					{
						if ( ! $$.mode_is( 'browse' ) )
							return;

						// Only change if the image is unzoomed.
						if ( $$.touch_manipulation.zoom.value != 100 )
							return;

						$$.inputs[ 'previous' ].click();
					} );
				}

				/**
					@brief		Are we in this mode?
					@since		2016-08-07 22:55:41
				**/
				$$.mode_is = function( mode )
				{
					return $$.mode == mode;
				}

                /**
                	@brief		Is this device a touch device?
                	@since		2016-11-04 18:37:26
                **/
                $$.is_touch_device = function()
                {
					try {
						document.createEvent("TouchEvent");
						return true;
					}
					catch (e)
					{
						return false;
					}
				}

				/**
					@brief		If we are in a blinking mode, blink to the previous image.
					@since		2015-09-07 21:23:24
				**/
				$$.maybe_blink_previous = function( image )
				{
					if ( ! $$.browse.blink_mode )
						return;

					$$.imagebuffer.stop_blinking();

					previous = image.previous;

					if ( $$.images.get_last().id == image.id )
					{
						previous = previous.previous;
					}
					else
						if ( image.previous.id == image.id )
							return;

					$$.imagebuffer.blink.enabled = $$.browse.blink_mode;

					// Load the previous previous.
					$$.show_image( previous );
				}

				/**
					@brief		Reload the current image so that its pointers point to the correct prev and next, in case the images has been reloaded.
					@since		2016-10-30 21:31:04
				**/
				$$.reload_current_image = function()
				{
					if ( $$.images.length == 0 )
						$$.current_image = null;
					if ( $$.current_image == null )
						return;
					// This is to ensure we load a current image if there are any.
					var id = $$.current_image.id;
					do
					{
						$$.current_image = $$.images.get_by_id( id );
						id--;
					}
					while ( $$.current_image === null );
				}

				$$.set_mode = function( new_mode )
				{
					$$.mode = new_mode;
					// Hide all mode buttons.
					$( '.mode', $$ ).hide();
					// And show only those buttons that we care about.
					$( '.mode.' + new_mode, $$ ).show();
				}

				/**
					@brief		Show the image at the current index.
					@since		2015-08-27 21:19:59
				**/
				$$.show_current_image = function()
				{
					if ( $$.current_image == null )
						return;
					$$.show_image( $$.current_image );
				}

				/**
					@brief		Show an image ID.
					@since		2015-09-07 19:50:16
				**/
				$$.show_image = function( image )
				{
					// Is there anything real to display?
					if ( image.data < 1 )
						return;
					var url = $$.data( 'source_url' );
					url = url.replace( 123456789, image.data  );
					$$.imagebuffer.display( url );
					$$.show_image_description();
				}

				/**
					@brief		Show the image description, if any.
					@since		2018-03-04 23:52:47
				**/
				$$.show_image_description = function( image_id = false )
				{
					if ( ! $$.mode_is( 'browse' ) )
						return;
					if ( ! image_id )
						image_id = $$.current_image.data;
					// Does this ID have a description?
					var description = $$.get_image_description( image_id );
					if ( description.length < 1 )
						return $$.$image_description.hide();
					$$.$image_description.html( description )
						.show();
				}

				/**
					@brief		Resize the big container div to fit the whole display.
					@since		2015-08-27 21:14:39
				**/
				$$.size_to_fit = function()
				{
					// Expand the div.
					$$.css( 'height', $$.window.height() );
					$$.css( 'width', $$.window.width() );
				}

                /**
                	@brief		Set and display the image with this id.
                	@since		2015-08-30 17:22:44
                **/
                $$.set_current_image = function( image )
                {
                	$$.current_image = image;
                	$$.show_current_image();
                	$$.update_counters();
                }

                /**
                	@brief		Animate the swiping and change the next image to the next image.
                	@since		2016-11-04 19:27:12
                **/
                $$.swipe_and_change = function( next_image, direction )
                {
					clearTimeout( $$.live.switch_timeout );

					var old_pan_position = $$.touch_manipulation.pan.positionX;

					var do_change = function()
					{
						$$.maybe_blink_previous( next_image );
						$$.set_current_image( next_image );
					}

					// Only animate if not zoomed in.
					if ( $$.touch_manipulation.zoom.value != 100 )
						return do_change();
					// And only animate on touch screens.
					if ( ! $$.is_touch_device() )
						return do_change();

					$( '.image.layer' )
					.animate( {
						'background-position' : old_pan_position + direction + '%',
						'opacity' : 0,
					}, 1000, 'swing', function()
						{
							$$.touch_manipulation.set_pan( old_pan_position, $$.touch_manipulation.pan.positionY );
							$( '.image.layer' ).css( 'opacity', 1 );
							do_change();
						}
					);
                }

                /**
                	@brief		Doess all of the footwork to switch to browse mode.
                	@since		2016-05-15 22:34:55
                **/
                $$.switch_to_browse_mode = function()
                {
					$$.set_mode( 'browse' );

					setTimeout( function()
					{
						// Fix the height of the browse box, because sometimes it's just a few pixels too large. On some devices.
						var $navigation_wrapper = $( '.navigation_wrapper' );

						// Only fix it once.
						if ( $navigation_wrapper.data( 'height_fixed' ) )
							return;
						$navigation_wrapper.data( 'height_fixed', true );

						var nav_height = $navigation_wrapper.outerHeight();
						var button_height = $( '.button', $navigation_wrapper ).first().outerHeight();
						var difference = nav_height - button_height;
						$( '.button', $navigation_wrapper ).css(
						{
							'padding-bottom' : difference / 2,
							'padding-top' : difference / 2,
						} );
					}, 100 );

					var image = $$.images.get_last().previous;

					$$.set_current_image( image );
					$$.maybe_blink_previous( image );
                }

                /**
                	@brief		Convenience method to switch to live mode.
                	@since		2016-06-01 23:00:19
                **/
                $$.switch_to_live_mode = function()
                {
                	if ( $$.mode_is( 'live' ) )
                		return;

                	clearTimeout( $$.live.switch_timeout );

					// Switch off blink.
					$$.imagebuffer.stop_blinking();
					$$.touch_source();
					$$.set_mode( 'live' );

					$$.inputs[ 'snapshot' ].show();

					// Switching to live mode should always show the last image.
					$$.show_image( { data : $$.live.max_index } );
                }

                /**
                	@brief		Take a snapshot of the current image.
                	@since		2016-05-15 22:35:24
                **/
                $$.take_snapshot = function()
                {
					// Telling PTV to keep the image will get us a new array of kept images, so no need to update any counters.
					$$.socket.emit( 'keep_image', {
						'source_id' : $$.source_id,
						'index' : $$.live.max_index,
					} );
                }

                /**
                	@brief		Touch the source, reactivating it.
                	@since		2015-08-30 20:28:03
                **/
                $$.touch_source = function()
                {
					$$.socket.emit( 'touch_source', {
						'source_id' : $$.source_id,
					} );
                }

                /**
                	@brief		Update the image counters in browse mode.
                	@since		2015-08-29 22:12:24
                **/
                $$.update_counters = function( index )
                {
					if ( $$.images.length < 1 )
					{
						$$.inputs.browse.hide();
						return;
					}
					else
						$$.inputs.browse.show();

                	if ( $$.current_image )
                	{
                		var text = $$.current_image.id;
                		if ( text == 'latest' )
                			text = '&#8212;';
						$$.current_counter.html( text );
					}

					// -1 to Ignore the latest.
                	$$.max_counter.html( $$.images.length - 1 );
                }

                /**
                	@brief		Sends an "update sources" message to the server.
                	@since		2016-08-09 13:08:31
                **/
                $$.update_sources = function()
                {
					var data = $$.emit_data();
					$$.socket.emit( 'update_sources', data );
                };

                /**
                	@brief		Convenience method to wait for a new live image.
                	@since		2016-08-09 13:03:26
                **/
                $$.wait_for_new_image = function( callback )
                {
                	var current_max = $$.live.max_index;

					if ( ! $$.live.updating )
					{
						$$.touch_source();
						$$.update_sources();
					}

                	var interval = setInterval( function()
                	{
                		if ( $$.live.max_index == current_max )
                			return;
                		clearInterval( interval );
                		callback();
                	}, 100 );
                }

                $$.wait_for_new_keepers = function( callback )
                {
                	var counter = 20;		// 2 * 100 ms

                	var old_length = $$.images.length;

                	var interval = setInterval( function()
                	{
                		counter--;
                		if ( counter == 0 )
                			old_length = -1;

                		if ( $$.images.length == old_length )
                			return;

                		clearInterval( interval );
                		callback();
                	}, 100 );
                }

                $$.init();
                // Debug. Keep this here since I use it so often.
                // $( '.menu.button' ).click();
			});
		}
	});
}(jQuery));
