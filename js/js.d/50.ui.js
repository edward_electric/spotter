/**
	@brief		Contains generic UI handling things like UI selects.
	@since		2016-11-15 19:28:45
**/
jQuery(function($)
{
	$.fn.extend(
	{
		ui : function()
		{
			return this.each(function ()
			{
				var $$ = $(this);

				$$.$ui_select = $( '#ui_select' ).change( function()
				{
					var val = $$.$ui_select.val();

					if ( localStorage )
						localStorage.setItem( 'pvspotter_preferred_ui', val )

					var selector = 'option[value="' + val + '"]';
					var option = $( selector, $$.$ui_select );
					var url = $( option ).data( 'url' );
					window.location = url;
				} );
			});
		}
	});
}(jQuery));
