/**
	@brief		Handles CSS zooming and panning using the Hammer library.
	@since		2016-10-31 13:08:51
**/
function touch_manipulation( options )
{
	var $$ = this;

	this.hammertime = null;			// The instance of the Hammer class.

	this.options = jQuery.extend( {
		'element' : '',				// The jQuery element(s) to manipulate.
		'enable_zoom' : true,		// Enable zooming.
		'enable_pan' : true,		// Enable panning.
		'root' : '',				// The root element to hook into.
	}, options );

	this.hammertime;				// Hammer object.

	this.pan = {};
	this.pan._deltaTime = 100000;	// Used internally to detect different pans.
	this.pan._positionX = 50;		// Used internally when panning.
	this.pan._positionY = 50;		// Used internally when panning.
	this.pan.positionX = 50;		// Start X when panning.
	this.pan.positionY = 50;		// Start Y when panning.

	/**
		@brief		Set the pan values.
		@since		2016-10-31 13:24:24
	**/
	this.set_pan = function( x, y )
	{
		new_position = x + '% ' + y + '%';
		this.options.element.css( 'background-position', new_position );
	}

	/**
		@brief		Contains info about the zoomed state.
		@since		2016-10-31 13:13:18
	**/
	this.zoom = {};
	this.zoom._deltaTime = 100000;	// Used internally to detect different pinches.
	this.zoom._start_zoom;			// Used internally to keep track of how much has been zoomed.
	this.zoom.value = 100;			// The percent of zoom (css background-size attribute).

	/**
		@brief		Return the zoom level of the image.
		@since		2016-10-31 13:13:59
	**/
	this.get_zoom = function()
	{
		return this.zoom.value;
	};

	/**
		@brief		Set the zoom level of the image.
		@since		2016-10-31 13:14:08
	**/
	this.set_zoom = function( new_value )
	{
		// Do not allow the user to zoom out.
		new_value = Math.max( 100, new_value );
		this.zoom.value = new_value;

		// When zooming 100%, show the whole image using a contain size.
		if ( new_value == 100 )
			new_value = 'contain';
		else
			new_value += '%';

		this.options.element.css( 'background-size', new_value );
	};

	/**
		@brief		Initialize the class.
		@since		2016-10-31 13:28:40
	**/
	this.init = function()
	{
		this.hammertime = new Hammer( $$.options.root );

		if ( this.options.enable_pan )
		{
			this.hammertime.on( 'pan', function( ev )
			{
				// Do not pan when zoomed out.
				if ( $$.get_zoom() == 100 )
					return;

				if ( ev.deltaTime < $$.pan._deltaTime )
				{
					// This looks like a new pinch.
					$$.pan._positionX = $$.pan.positionX;
					$$.pan._positionY = $$.pan.positionY;
				}

				$$.pan.positionX = $$.pan._positionX - ( ev.deltaX * 0.1 );
				$$.pan.positionY = $$.pan._positionY - ( ev.deltaY * 0.1 );

				$$.pan.positionX = Math.max( 0, $$.pan.positionX );
				$$.pan.positionX = Math.min( 100, $$.pan.positionX );
				$$.pan.positionX = Math.round( $$.pan.positionX, 0 );

				$$.pan.positionY = Math.max( 0, $$.pan.positionY );
				$$.pan.positionY = Math.min( 100, $$.pan.positionY );
				$$.pan.positionY = Math.round( $$.pan.positionY, 0 );

				$$.set_pan( $$.pan.positionX, $$.pan.positionY );
				$$.pan._deltaTime = ev.deltaTime;
			});
		}

		if ( this.options.enable_zoom )
		{
			this.hammertime.get( 'pinch' ).set( { 'enable': true } );

			this.hammertime.on( 'pinch', function( ev )
			{
				if ( ev.deltaTime < $$.zoom._deltaTime )
				{
					// This looks like a new pinch.
					$$.zoom._start_zoom = $$.get_zoom();
				}

				var new_zoom = ev.scale * $$.zoom._start_zoom;
				$$.set_zoom( new_zoom );
				$$.zoom._deltaTime = ev.deltaTime;

				if ( $$.get_zoom() == 100 )
					$$.set_pan( 50, 50 );
			});
		}
	}

	this.init();

	return this;
};
