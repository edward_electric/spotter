jQuery(function($)
{
	$.fn.extend(
	{
		index : function()
		{
			return this.each(function ()
			{
				var $$	= $(this)

				$$.socket = ptv_socket_connect();

				$$.sources = {};

				$$.socket.on( 'connect', function()
				{
					var sources = $( 'img.source', $$ ).toArray();
					var source_ids = [];
					for( counter = 0; counter < sources.length ; counter++ )
					{
						$source = $( sources[ counter ] );
						var source_id = $source.data( 'source_id' );
						$$.sources[ source_id ] = $source;
						source_ids.push( source_id );
					}
					/**
						$$.socket.emit( 'touch_source', {
							'source_id' : source_id,
						} );
					**/
					$$.socket.emit( 'update_sources', {
						'source_ids' : source_ids
					} );
				} );

				$$.socket.on( 'new_image', function( data )
				{
					$source = $$.sources[ data.id ];

					if ( $source === undefined )
						return;

					var url = $source.data( 'image_url' );
					url = url.replace( 123456789, data.index );
					$source.prop( 'src', url );
				} );

				$$.socket.emit( 'connect' );
			});
		}
	});
}(jQuery));
