function linked_list()
{
	/**
		@brief		Append some data under a specific id / index.
		@since		2015-08-28 21:50:19
	**/
	this.append = function( id, data )
	{
		// Dupe check.
		if ( this._index.id !== undefined )
			return;

		var node =
		{
			counter: this.length + 1,
			data: data,
			id : id,
			next: null,
			previous : null,
		};

		if ( this._first === null )
		{
			this._first = node;
			this._last = node;
			node.previous = node;
			node.next = node;
		}
		else
		{
			// We are the previous item's next.
			node.previous = this._last;
			node.previous.next = node;

			// Our next is the first image.
			node.next = this._first;

			// The next's previous is this.
			node.next.previous = node;

			// And we are the last one.
			this._last = node;
		}

		this._index[ id ] = node;
		this.length++;
	};

	/**
		@brief		Return an item.
		@since		2015-08-30 15:32:23
	**/
	this.get_by_id = function( id )
	{
		if ( typeof( this._index[ id ] ) != 'undefined' )
			return this._index[ id ];
		return null;
	}

	/**
		@brief		Return the first image, if any.
		@since		2015-09-07 19:56:22
	**/
	this.get_first = function()
	{
		return this._first;
	}

	// Return the last image, if any.
	this.get_last = function()
	{
		return this._last;
	}

	/**
		@brief		Remove all of the items.
		@since		2015-08-29 22:05:31
	**/
	this.flush = function()
	{
		this._index = {};
		this._first = null;
		this._last = null;
		this.length = 0;
	}

	this.flush();

	return this;
};
