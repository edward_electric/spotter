/**
	@brief		An image double buffer.
	@since		2015-08-28 21:13:32
**/
function double_buffer( $base )
{
	var $$ = this;

	$$.active = 0;			// Index of active image.
	$$.$base = $base;			// Base div.

	$$.blink = {				// Make images blink / switch between each other.
		'enabled' : false,
		'delay' : 1000,			// Milliseconds to wait between blinks.
		'interval' : null,		// Blink interval.
	};

	$$.buffers = [];			// Array of image divs / buffers.
	$$.busy_until = 0;			// How long is the double buffer expected to be busy?

	$$.max_buffers = 2;			// How many buffers to use.

	$$.queue = {};				//
	$$.queue.max = 2;			// Maximum queue size. Should be at least 1 in order to be able to load a blink.
	$$.queue.urls = [];			// URLs to load.

	/**
		@brief		Initialization.
		@since		2015-08-28 21:50:10
	**/
	$$.init = function()
	{
		// Add the two buffers.
		$$.buffers[ $$.active ] = $( '<div>' ).addClass( 'image layer one' ).appendTo( $$.$base );
		$$.next();		// Go to 1
		$$.buffers[ $$.active ] = $( '<div>' ).addClass( 'image layer two' ).appendTo( $$.$base );
		$$.next();		// And back to 0
	}

	/**
		@brief		Return the current buffer.
		@since		2015-08-28 21:21:15
	**/
	$$.current = function()
	{
		return $$.buffers[ $$.active ];
	}

	/**
		@brief		Display an image in the next buffer.
		@since		2015-08-28 21:19:08
	**/
	$$.display = function( url )
	{
		var utc = $$.utc();

		if ( utc < $$.busy_until )
		{
			// Queue full? The oldest url gets dumped.
			if ( $$.queue.urls.length >= $$.queue.max )
				$$.queue.urls.shift();

			// Put this url at the end of the queue.
			$$.queue.urls.push( url );

			return;
		}

		// Display has timed out.
		if ( $$.queue.urls.length > 0 )
			return $$.process_queue();

		$$.busy_until = utc + 5;

		/**
			@brief		Seems to work.
			@since		2016-11-01 17:40:32
		**/
		$( '<img>' )
			.addClass( 'preloader' )
			.appendTo( $$.$base )
			.load( function()
			{
				var img = this;
				setTimeout( function()
				{
					css_url = 'url( ' + url + ')';
					$$.current()
						.css( 'background-image', css_url )
					setTimeout( function()
					{
						$$.busy_until = 0;
						$$.make_current_top()
						$$.maybe_blink();
						$$.process_queue();
						$( img ).remove();
					}, 50 );
				}, 50 );
			} )
			.attr( 'src', url );
	}

	/**
		@brief		Maybe enable blinking between buffers.
		@since		2015-09-07 20:01:48
	**/
	$$.maybe_blink = function()
	{
		if ( ! $$.blink.enabled )
			return;
		if ( $$.queue.urls.length > 0 )
			return;
		$$.stop_blinking();
		$$.blink.interval = setInterval( function()
		{
			$$.make_current_top();
		}, $$.blink.delay );
	}

	/**
		@brief		Make the current buffer the top buffer.
		@since		2015-09-07 20:03:15
	**/
	$$.make_current_top = function()
	{
		$$.current().addClass( 'top' );
		$$.next().removeClass( 'top' );
	}

	/**
		@brief		Return the next buffer index.
		@since		2015-08-28 21:18:52
	**/
	$$.next = function()
	{
		$$.active = $$.active + 1;
		if ( $$.active >= ( $$.max_buffers ) )
			$$.active = 0;
		return $$.current();
	}

	/**
		@brief		Process the queue.
		@since		2016-11-01 09:53:17
	**/
	$$.process_queue = function()
	{
		if ( $$.queue.urls.length < 1 )
			return;
		var url = $$.queue.urls.shift();
		$$.display( url );
	}

	/**
		@brief		Stop the blinking already.
		@since		2015-09-07 21:25:10
	**/
	$$.stop_blinking = function()
	{
		if ( $$.blink.interval !== null )
			clearInterval( $$.blink.interval );
		$$.blink.interval = null;
		$$.blink.enabled = false;
	}

	/**
		@brief		Return the unix time.
		@since		2015-09-01 20:20:01
	**/
	$$.utc = function()
	{
		return Math.floor(Date.now() / 1000);
	}

	$$.init();

	return $$;
};
