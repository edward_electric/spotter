/**
	@brief		Helper class for handling audio snapshotting.
	@since		2016-11-10 23:13:12
**/
function ui_audio_processor()
{
	$$ = this;

	$$.callbacks = {			// Arrays of various callbacks.
		'bang' : {},			// There was a bang (clip) detected.
		'off' : {},				// Audio processor was switched off.
		'on' : {},				// Audio processor was switched on.
		'volume' : {},			// There was a sound of some kind.
	};
	$$.processor = false;		// Our audioprocessor.
	$$.sensitivity = 0;			// When to record a bang.
	$$.volume = 0;				// The maximum measured volume.
	$$.old_volume = 0;			// Temp. For delta work.
	$$._volume_timeout = 0;		// Timeout for clearing the volume.

	/**
		@brief		Add a callback.
		@since		2016-11-10 23:44:31
	**/
	$$.add_callback = function( type, id, callback )
	{
		if ( $$.callbacks[ type ] === undefined )
			$$.callbacks[ type ] = {};
		$$.callbacks[ type ][ id ] = callback;
	}

	/**
		@brief		Do the callback array.
		@since		2016-11-10 23:42:32
	**/
	$$.do_callback = function( type, parameters )
	{
		for ( var id in $$.callbacks[ type ] )
		{
			var callback = $$.callbacks[ type ][ id ];
			callback( parameters );
		}
	}

	/**
		@brief		Return the volume of the meter, if possible.
		@since		2016-11-11 00:33:14
	**/
	$$.get_volume = function()
	{
		return $$.volume;
	}

	/**
		@brief		Sets how sensitive the processor is. This varies greately between devices, so it doesn't really make too much sense but gives the user some choices.
		@since		2016-11-10 23:15:07
	**/
	$$.set_sensitivity = function( new_sensitivity )
	{
		if ( $$.sensitivity == new_sensitivity )
			return;

		// Is it currently on?
		if ( $$.sensitivity > 0 )
		{
			$$.sensitivity = new_sensitivity;

			if ( new_sensitivity == 0 )
			{
				$$.switch_off();
				$$.do_callback( 'off' );
			}
			else
				$$.meter.clipLevel = new_sensitivity;
		}
		else
		{
			// It is currently off. Switch on.
			try
			{
				$$.sensitivity = new_sensitivity;
				$$.switch_on();
				$$.do_callback( 'on' );
			}
			catch ( e )
			{
				$$.sensitivity = 0;
				$$.do_callback( 'off' );
				throw e;
			}
		}
	}

	/**
		@brief		Switch off the meter if it happens to be on.
		@since		2016-11-10 23:20:47
	**/
	$$.switch_off = function()
	{
		if ( ! $$.processor )
			return;
		$$.processor.disconnect();
		$$.processor.onaudioprocess = null;
		$$.processor = false;
	}

	/**
		@brief		Attempt to switch the meter on.
		@since		2016-11-10 23:56:05
	**/
	$$.switch_on = function()
	{
		// Only bang at most once per second.
		var ready_for_bang = true;

		window.AudioContext = window.AudioContext || window.webkitAudioContext;
		audioContext = new AudioContext();

		// monkeypatch getUserMedia
		navigator.getUserMedia =
		navigator.getUserMedia ||
		navigator.webkitGetUserMedia ||
		navigator.mozGetUserMedia;

		// ask for an audio input
		navigator.getUserMedia(
		{
			"audio" : true
		},

		function( stream )
		{
			$$.processor = audioContext.createScriptProcessor();
			$$.processor.onaudioprocess = function( event )
			{
				var array = event.inputBuffer.getChannelData( 0 );

				$$.do_callback( 'process_buffer', { 'buffer' : array } );

				var average = 0;

				// get all the frequency amplitudes
				for (var i = 0; i < array.length; i++)
				{
					value = array[i];
					average += Math.abs( value );
				}

				average = average / array.length;
				if ( average > $$.volume )
				{
					$$.old_volume = $$.volume;
					$$.volume = average;
					$$.do_callback( 'volume', { 'volume' : $$.get_volume(), 'old_volume' : $$.old_volume } );
					clearInterval( $$._volume_timeout );
					$$._volume_timeout = setInterval( function()
					{
						if ( $$.volume > 0 )
						{
							$$.volume = $$.volume / 2;
							$$.old_volume = $$.volume;
							$$.do_callback( 'volume', { 'volume' : $$.get_volume(), 'old_volume' : $$.old_volume } );
						}
					}, 2000 );
				}

				if ( average > $$.sensitivity )
				{
					if ( ready_for_bang )
					{
						ready_for_bang = false;
						$$.do_callback( 'bang' );
						setTimeout( function()
						{
							ready_for_bang = true;
						}, 1000 );
					}
				}
			}

			$$.processor.connect( audioContext.destination );

			mediaStreamSource = audioContext.createMediaStreamSource( stream );
			mediaStreamSource.connect( $$.processor );
		},

		function()
		{
			throw 'Stream generation failed.';
		}

		);
	}

	return this;
};
