/**
	@brief		An easier way to interface with the source.
	@since		2016-08-08 15:42:00
**/
function source_api( $base )
{
	this.$base = $base;			// Base div.

	/**
		@brief		The CSRF token.
		@since		2016-08-08 15:46:27
	**/
	this.csrf = '';

	/**
		@brief		The API url for this source.
		@since		2016-08-08 15:46:37
	**/
	this.url = '';

	/**
		@brief		Get the source settings.
		@since		2016-08-08 15:48:14
	**/
	this.load = function( callback )
	{
		$.ajax( {
			'dataType' : 'json',
			'method' : 'GET',
			'url' : this.url
		} )
		.done( function( data )
		{
			callback( data );
		} );
	}

	/**
		@brief		Init.
		@since		2016-08-08 15:42:28
	**/
	this.init = function()
	{
		this.url = $base.data( 'source_api' );
		this.csrf = $base.data( 'csrf' );
	}

	/**
		@brief		Send the new settings for the source.
		@since		2016-08-08 15:48:01
	**/
	this.store = function( new_settings, callback )
	{
		var data = jQuery.extend( new_settings, {
			'_csrf_token' : this.csrf
		} );
		$.ajax( {
			'contentType' : 'application/json',
			'data' : JSON.stringify( data ),
			'method' : 'PUT',
			'processData' : false,
			'url' : this.url
		} )
		.done( function()
		{
			if ( callback !== undefined )
				callback();
		} );
	}

	this.init();

	return this;
};
