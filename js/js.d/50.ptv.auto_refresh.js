/**
	@brief		Auto refresh a div by reloading the page.
	@since		2016-11-06 18:55:23
**/
jQuery(function($)
{
	$.fn.extend(
	{
		ptv_auto_refresh : function()
		{
			return this.each(function ()
			{
				var $$ = $(this);

				$$.classes = '.' + $$.prop( 'class' ).replace( /\ /g, '.' );
				$$.interval = false;		// The refresh interval object.
				$$.refresh_seconds = Math.max( $$.data( 'auto_refresh_seconds' ), 5 );

				/**
					@brief		Refresh the page.
					@since		2016-11-06 18:56:47
				**/
				$$.refresh = function()
				{
					$.ajax( {
						'method' : 'GET'
					} )
					.done( function( data )
					{
						var $data = $( data );
						var $div = $( $$.classes, $data );

						// Did you find the new div?
						if ( $div.length < 1 )
							return;

						$$.html( $div.html() );
					} );
				}

				$$.interval = setInterval( function()
				{
					$$.refresh();
				}, $$.refresh_seconds * 1000 );
			});
		}
	});
}(jQuery));
