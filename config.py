## Directories
## -----------

# This is the directory that contains all temporary files that aren't supposed to last for more than a restart.
import os.path

DIRECTORIES = {}
DIRECTORIES[ 'BASE' ] = os.path.dirname( os.path.realpath(__file__) )
DIRECTORIES[ 'SCRIPTS' ] = DIRECTORIES[ 'BASE' ] + '/scripts'
DIRECTORIES[ 'TEMP' ] = DIRECTORIES[ 'BASE' ] + '/temp'
DIRECTORIES[ 'IMAGES' ] = DIRECTORIES[ 'TEMP' ] + '/images'
DIRECTORIES[ 'KEEPERS' ] = DIRECTORIES[ 'TEMP' ] + '/keepers'

# Complete path, including filename, of fswebcam program.
FSWEBCAM_PATH = '/usr/bin/fswebcam'

## The name of the git server from where to update.
# @since		2017-12-03 12:33:47
GIT_SERVER = 'git.plaintargetview.com'

# Where to find the imagemagick bin files.
IMAGEMAGICK_PATH = '/usr/bin'

## How many old, unsaved images to keep.
# Used to keep the amount of images in check.
# @since		2016-09-16 18:52:51
KEEP_IMAGES = 10

## The version of the PTV software.
# @since		2016-11-10 19:17:57
PTV_VERSION = "1.20180506"

## This is the script that is run when the camera stops responding.
# @since		2016-11-23 18:20:19
RESET_SCRIPT = DIRECTORIES[ 'SCRIPTS' ] + '/reset_cameras.sh'

## Where to store the session files.
# @since		2016-10-31 22:03:40
SESSION_FILE_DIR = DIRECTORIES[ 'TEMP' ] + '/sessions'

## Which UIs are available to the user?
# @since		2016-10-30 16:12:56
UIS_AVAILABLE = {
	'classic' :
	{
		'description' : 'Simple and fully-featured. The first interface created.',
		'nice_name' : 'Classic',
	},
}

## The default UI to use when the user has not set one in his localstorage.
# @since		2016-10-30 16:04:27
UI_DEFAULT = 'classic'
