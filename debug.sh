#!/bin/bash
. venv/bin/activate venv
while ( true ) ; do
	clear
	date
	# Delete the pyc files
	find src -iname *.pyc -exec rm -v {} \;
	python debug.py
	sleep 1
done
