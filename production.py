from src import app

if __name__ == "__main__":
	app.set_debug( False )
	app.socketio.run( app, host='0.0.0.0' )
