from flask_socketio import SocketIO
from src import app

if __name__ == "__main__":
	app.set_debug( True )
	app.socketio.run( app, host='0.0.0.0' )
