from src import app
from flask import redirect, request, session, url_for
from flask_babel import Babel, gettext

class Language( Babel ):

	available_languages = {
		'en' : 'English',
		'sr' : 'Srbski',
		'sv' : 'Svenska',
	}

	## The name of the language cookie set.
	# @since		2015-08-22 14:16:46
	language_key = 'preferred_language'

	## Return the available languages as options for use in the UI lang switcher.
	# @since		2017-01-20 01:20:15
	def get_language_options( self ):
		r = []

		# The first option is always to no language.
		lang_string = "<option data-url=\"" + url_for( 'switch_language', lang = '' ) + "\">" + gettext( 'Let my device choose' ) + "</option>"
		r.append( lang_string )

		# And not put in all of the real choices.
		current_locale = self.get_locale()
		for language in self.available_languages:
			if language == current_locale:
				selected = " selected"
			else:
				selected = ""

			lang_string = "<option data-url=\"{}\"{}>{}</option>".format(
				url_for( 'switch_language', lang = language ),
				selected,
				self.available_languages[ language ]
			)

			r.append( lang_string )

		return ''.join( r )

	## Return the current locale, or the best match.
	# @since		2017-01-20 01:18:05
	def get_locale( self ):
		preferred_language = session.get( self.language_key )
		if preferred_language is None:
			return request.accept_languages.best_match( self.available_languages )
		return preferred_language

	## Return the nice name of the language.
	# @since		2016-11-14 16:20:42
	def get_name( self, lang ):
		return self.available_languages[ lang ]

	def init( self, app ):

		self.app = app

		@app.route( '/lang/' )
		@app.route( '/lang/<string:lang>' )
		def switch_language( lang = '' ):
			if lang in self.available_languages:
				session[ self.language_key ] = lang
			else:
				if self.language_key in session:
					# Delete the language cookie.
					del session[ self.language_key ]

			return redirect( url_for( 'route_app_index' ) )

		@self.localeselector
		def get_locale():
			return self.get_locale()
