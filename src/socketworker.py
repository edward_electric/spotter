from src import app
from flask_socketio import emit
import socket

class SocketWorker():

	## A source has a new image. Inform the users.
	# @since		2015-08-21 14:12:09
	def new_image_notification( self, source ):
		app.socketio.emit( 'new_image',
		{
			'id' : source.id,
			'index' : source.images.get_latest_key(),
		} )

	def init( self ):

		## Check for a valid internet connection. This is mostly used to check for update capability.
		# @since		2017-12-03 12:32:01
		@app.socketio.on( 'check_internet_access' )
		def socket_check_internet_access( data = None ):
			try:
				result = socket.getaddrinfo( app.config[ 'GIT_SERVER' ], 80 )
				app.socketio.emit( 'internet_access',
				{
					'status' : True,
				} )
			except:
				app.socketio.emit( 'internet_access',
				{
					'status' : False,
				} )

		@app.socketio.on( 'connect' )
		def socket_connect( data = None ):
			app.sources.init_threads()

		## Return all of the keepers for this source.
		# @since		2016-10-30 20:44:58
		@app.socketio.on( 'get_keepers' )
		def socket_get_keepers( data ):
			source = app.sources.source( data.get( 'source_id' ) )
			if source is None:
				return

			source.emit_keepers()

		# Make the specified image a snapshot image.
		@app.socketio.on( 'keep_image' )
		def socket_keep_image( data ):
			source = app.sources.source( data.get( 'source_id' ) )
			if source is None:
				return

			image = source.images.get( data.get( 'index' ) )
			if image is None:
				return;

			source.keep_image( data.get( 'index' ) )
			source.emit_keepers()

		## Set the description of an image.
		# @since		2018-03-04 23:33:53
		@app.socketio.on( 'set_image_description' )
		def socket_keep_image( data ):
			source = app.sources.source( data.get( 'source_id' ) )
			if source is None:
				return

			image = source.images.get( data.get( 'index' ) )
			if image is None:
				return;

			description = data.get( 'image_description' ).encode("utf-8")
			image.set_description( description )

		## Touch a source.
		# @since		2015-08-30 18:48:40
		@app.socketio.on( 'touch_source' )
		def socket_touch_source( data ):
			source = app.sources.source( data.get( 'source_id' ) )
			if source is None:
				return
			source.touch()

		# Trash the specified image.
		@app.socketio.on( 'trash_image' )
		def socket_trash_image( data ):
			source = app.sources.source( data.get( 'source_id' ) )
			if source is None:
				return

			image = source.images.get( data.get( 'index' ) )
			if image is None:
				return;

			source.trash_image( data.get( 'index' ) )
			source.emit_keepers()

		## Update the PTV system via git.
		# @since		2017-12-03 13:15:14
		@app.socketio.on( 'update_ptv' )
		def socket_update_ptv( data = None ):
			app.update()
			app.socketio.emit( 'ptv_updated' )

		# Update all sources.
		@app.socketio.on( 'update_sources' )
		def socket_update_sources( data ):
			source = app.sources.source( data.get( 'source_id' ) )
			if source is not None:
				source.touch()
			# Touch multiple sources, if asked.
			for source_id in data.get( 'source_ids', [] ):
				source = app.sources.source( source_id )
				if source is not None:
					source.touch()
			app.sources.capture()
