from flask import jsonify, url_for, request
from threading import Thread
from src import app

import json
import re
import ssl
import time
import urllib2

## The neighborhood watch looks for other PlainTargetViews on the network.
# @since		2016-11-06 20:54:42
class Neighborhood_Watch( dict ):

	def check_ip( self, **kwargs ):

		ip = kwargs.get( 'ip' )

		# Here is a hack, since I don't understand contexts well enough.
		# Before creating the context, we set the SERVER_NAME, which was saved previously.
		old_server_name = app.config[ 'SERVER_NAME' ]
		app.config[ 'SERVER_NAME' ] = self.http_host

		# Allow self-signed certs.
		context = ssl._create_unverified_context()

		with app.app_context():

			# Start with our own url
			base_url = url_for( 'route_sources_api_sources', _external = True )
			# Remove the counter, since <no value> will be the same as localhost.
			base_url = re.sub( '[0-9]+\.', '.', base_url, 1 )

			# After calling url_for, we clear the server_name in order to prevent Flask from serving us up for other domains on the same IP.
			app.config[ 'SERVER_NAME' ] = old_server_name

			# Instead of 0, look at the base url.
			if ip == 0:
				ip = ''
			if self.get( ip ) is not None:
				del self[ ip ]
			url = base_url.replace( '.', str( ip ) + '.', 1 )
			try:
				reply = urllib2.urlopen( url, context = context )
				data = json.load( reply )

				# Was the json correctly loaded?
				if type ( data ) is not dict:
					return
				self[ data[ 'id' ] ] = data[ 'sources' ]
			except Exception as e:
				# Something went wrong. Whatever it was, it's preventing us from listing this PTV's sources.
				pass

	## Init a watcher for this net.
	# @since		2016-11-06 20:55:28
	def init( self ):

		# Use the counter to only keep this thread alive for as long as someone is interested in watching.
		self.counter = 6

		# Is there already a thread?
		if app.threads[ 'neighborhood_watch' ] is not None:
			return

		self.http_host = request.environ[ 'HTTP_HOST' ]

		thread = Thread( target = self.look_around_loop )
		app.threads[ 'neighborhood_watch' ] = thread
		thread.start()

	## Loop the search function.
	# @since		2016-11-06 21:02:54
	def look_around_loop( self ):
		while self.counter > 0:
			self.look_around()
			time.sleep( 5 )
			self.counter = self.counter - 1

		# We've run out of counter, kill the thread reference.
		app.threads[ 'neighborhood_watch' ] = None

	## Look for other PTVs
	# @since		2016-11-09 17:22:30
	def look_around( self ):
		for ip in range( 0, 100 ):
			thread = Thread( target = self.check_ip, kwargs = { 'ip' : ip } )
			thread.start()
