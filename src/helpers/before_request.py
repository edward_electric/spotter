from src import app
from flask import g, request, session

@app.before_request
def set_app():
    g.app = app

@app.before_request
def csrf_protect():
	# If we're posting, check the token.
	if request.method == "POST":
		token = session.get( '_csrf_token', None )
		if not token or token != request.form.get( '_csrf_token' ):
			abort(403)
	else:
		# If not posting, check for its existence.
		if session.get( '_csrf_token', '' ) == '':
			import md5
			import random
			random_number = random.random()
			session['_csrf_token'] = md5.new( str( random_number ) ).hexdigest()
	# Always insert the token into the template engine.
	app.jinja_env.globals['csrf_token'] = session['_csrf_token']
