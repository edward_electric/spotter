from src import app
from flask import url_for
from os import path
import os

@app.context_processor
def filestat():
	def filestat( filename ):
		mtime = path.getmtime( os.getcwd() + os.sep + 'static/' + filename )
		return url_for( 'static', filename = filename ) + '?' + str( mtime )
	return dict( filestat = filestat )
