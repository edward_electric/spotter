from email.Utils import formatdate
from flask import Flask, render_template, redirect, session, url_for
from flask_session import Session
from werkzeug.contrib.cache import SimpleCache

import datetime
import json
import os.path
import random
import sys
import time

class PlainTargetView( Flask ):

	## Initialize ourself.
	# @since		2014-07-21 08:30:41
	def init( self ):
		self.load_config()

		self.config['SESSION_TYPE'] = 'filesystem'

		self.secret_key = os.urandom( 64 )

		self.set_debug( True )

		# Do path and sanity checking.
		self.check_executables()
		self.check_temp()
		self.clean_temp()

		self.threads = {}
		self.threads[ 'update_source' ] = {}
		self.threads[ 'neighborhood_watch' ] = None

		# All done. We are ready to begin.
		# Note how we now can import things that are related to PTV, instead of doing it up top.

		self.cache = SimpleCache()
		self.session = Session( self )

		from sources import sources
		self.sources = sources.Sources()

		self.setup_routes()
		import helpers.context_processors
		import helpers.before_request

		from i18n import language
		self.babel = language.Language( self )
		self.babel.init( self )

		from socketworker import SocketWorker
		self.socketworker = SocketWorker()
		self.socketworker.init()

		from neighborhood_watch import Neighborhood_Watch
		self.neighborhood_watch = Neighborhood_Watch()

		self.reset()

	## Check the executable files.
	# @since		2014-07-23 23:51:08
	def check_executables( self ):
		IM = self.config[ 'IMAGEMAGICK_PATH' ]
		commands = [ 'compare', 'convert' ]
		for command in commands:
			# Check for existence of this command.
			filename = IM + '/' + command
			if not os.path.isfile( filename ):
				sys.exit( "Imagemagick {} not found in path: {}".format( command, filename ) )

		filename = self.config[ 'FSWEBCAM_PATH' ]
		if not os.path.isfile( filename ):
			sys.exit( "fswebcam not found in path: {}".format( filename ) )

	## Check the temp directory
	# @since		2014-07-23 23:54:49
	def check_temp( self ):
		# Does it exist?
		dir = self.config[ 'DIRECTORIES' ][ 'TEMP' ]
		if not os.path.isdir( dir ):
			os.mkdir( dir )
		# Now try to write to it
		if  not os.access( dir, os.W_OK):
			sys.exit( "No write access to directory {}".format( dir ) )
		# End of temp directory handling.

	## Remove all files from the temp directory
	# Except for the log file.
	# @since		2014-08-09 21:55:03
	def clean_temp( self ):
		self.delete_files( self.config[ 'DIRECTORIES' ][ 'TEMP' ] )
		self.populate_temp()

	## Delete all of the files in this subdirectory.
	# @since		2016-11-01 16:08:32
	def delete_files( self, dir ):
		import glob
		for file in glob.glob( dir + '/*' ):
			if os.path.isfile( file ):
				os.remove( file )
			else:
				self.delete_files( file )

	## Return an ID for this instance of PTV
	# @since		2016-11-08 20:04:03
	def get_id( self ):
		import md5
		m = md5.new()
		m.update( 'instance' )
		m.update( self.secret_key )
		return m.hexdigest()

	## Return a complete temporary file name.
	# kawgs = prefix, suffix and dir.
	# @since		2015-08-16 09:06:41
	def get_temp_filename( self, **kwargs ):
		import tempfile
		file = tempfile.NamedTemporaryFile(
			prefix = kwargs.get( 'prefix', 'temp_' ),
			suffix = kwargs.get( 'suffix', '.tmp' ),
			dir = kwargs.get( 'dir', self.config[ 'DIRECTORIES' ][ 'TEMP' ] ),
			delete = False
		)
		filename = file.name
		file.close
		return filename

	## Return a sorted list of UIs
	# @since		2016-11-15 21:03:32
	def get_uis( self ):
		return sorted( self.config[ 'UIS_AVAILABLE' ] )

	## Return the path to an imagemagick command.
	# @since		2014-07-25 19:33:57
	def imagemagick( self, command ):
		return "{}{}{}".format( self.config[ 'IMAGEMAGICK_PATH' ], os.sep, command )

	## Try to load the config
	# @since		2014-07-22 22:42:38
	def load_config( self ):
		configfile = os.getcwd() + os.sep + 'config.py'
		configfile = os.path.realpath( configfile )
		if not os.path.isfile( configfile ):
			sys.exit( 'Configfile {} not found.'.format( configfile ) )
		self.config.from_pyfile( configfile )

	## Remove characters from a string that are completely crazy.
	# @since		2018-03-05 20:37:39
	def normalize_input_string( self, string ):
		string = string.replace( '/', '-' )
		string = string.replace( '\\', '-' )
		return string

	## Set up the directories in temp.
	# @since		2016-10-31 21:51:11
	def populate_temp( self ):
		dirs = self.config[ 'DIRECTORIES' ]
		# Reverse them to create TEMP first and then all its subdirs.
		keys = list( reversed( self.config[ 'DIRECTORIES' ].keys() ) )
		for key in keys:
			if not os.path.isdir( dirs[ key ] ):
				os.mkdir( dirs[ key ] )

	## Process any threading.
	# @since		2016-11-01 13:27:45
	def process_thread( self ):
		import time
		while True:
			time.sleep( 0.1 )
			self.sources.capture()

	## Reboot the system.
	# Requires system permission to run the reboot command.
	# @since		2016-09-25 13:16:31
	def reboot( self ):
		from gevent.subprocess import Popen, PIPE
		p = Popen( [
			'sudo',
			'/sbin/shutdown',
			'-r',
			'now'
		], stdout=PIPE, stderr=PIPE );
		err, out = p.communicate()

	## Reset the spotter.
	# @since		2014-07-30 22:25:57
	def reset( self ):
		self.clean_temp()
		self.load_config()
		self.sources.init()

	## Set up debug mode and its associated logging.
	# @since		2016-11-07 17:23:21
	def set_debug( self, value ):

		self.debug = value

		import logging
		from logging.handlers import RotatingFileHandler

		handler = RotatingFileHandler( self.config[ 'DIRECTORIES' ][ 'TEMP' ] + '/plaintargetview.log', maxBytes=10000, backupCount=1 )
		if value:
			handler.setLevel( logging.DEBUG )
		else:
			handler.setLevel( logging.INFO )

		self.logger.addHandler(handler)

	## Setup the routes.
	# @since		2015-08-16 22:21:08
	def setup_routes( self ):
		@self.route( '/' )
		def route_app_index():
			# Is there a webcam available? Go directly to it.
			if len( self.sources ) == 1:
				source_id = self.sources.keys()[ 0 ]
				return redirect( url_for( 'route_sources_source_view_auto' , source_id = source_id ) )
			# Show the overview instead, to let the user choose.
			return redirect( url_for( 'route_app_camera_select' ) )

		## Show the user a list of cameras and neighboring systems to choose from.
		# @since		2016-11-06 18:59:24
		@self.route( '/camera_select' )
		def route_app_camera_select():
			self.neighborhood_watch.init()
			return render_template( 'camera_select.html' )

		@self.errorhandler( 404 )
		def not_found(error):
			return redirect( url_for( 'route_app_index' ) )

		@self.route( '/reboot' )
		def route_app_reboot():
			return render_template( 'reboot.html' )

		@self.route( '/reboot_now' )
		def route_app_reboot_now():
			self.reboot()

		@self.route( '/reset' )
		def route_app_reset():
			self.reset()
			return redirect( url_for( 'route_app_index' ) )

		@self.route( '/shutdown' )
		def route_app_shutdown():
			return render_template( 'shutdown.html' )

		@self.route( '/shutdown_now' )
		def route_app_shutdown_now():
			self.shutdown()

		self.sources.setup_routes()

	## Shutdown the system.
	# Requires system permission to run the shutdown command.
	# @since		2016-09-25 13:16:31
	def shutdown( self ):
		from gevent.subprocess import Popen, PIPE
		p = Popen( [
			'sudo',
			'/sbin/shutdown',
			'now'
		], stdout=PIPE, stderr=PIPE );
		err, out = p.communicate()

	## Return the current datetime in a pleasing format.
	# @since		2016-08-07 15:36:28
	def time( self ):
		dt = datetime.datetime.now()
		return formatdate( float( dt.strftime( '%s' ) ) )

	## Try to update
	# @since		2016-11-11 13:29:35
	def update( self ):
		from gevent.subprocess import Popen, PIPE

		# First reset any local changes
		p = Popen( [
			'/usr/bin/git',
			'reset',
			'--hard',
		], stdout=PIPE, stderr=PIPE );
		err, out = p.communicate()

		# And now pull a new version
		p = Popen( [
			'/usr/bin/git',
			'pull',
		], stdout=PIPE, stderr=PIPE );
		err, out = p.communicate()

	## Return the utc unix time.
	# @since		2015-08-13 19:05:24
	def utc( self ):
		return int( time.time() )
