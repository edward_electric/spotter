from flask_socketio import SocketIO
from gevent import monkey
from src.plaintargetview import PlainTargetView

import os

# This is necessary for gevent to allow forking to fswebcam.
monkey.patch_all()

# Our base dir should be one higher than the src dir.
base_dir = os.path.dirname(__file__)
base_dir = os.path.dirname( base_dir )

app = PlainTargetView( __name__, static_folder = base_dir + '/static', template_folder = base_dir + '/templates' )
app.socketio = SocketIO( app )
app.init()
