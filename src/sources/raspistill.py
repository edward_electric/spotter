import glob
import os
import re
from image import Image
from src import app
from source import Source
from gevent.subprocess import Popen, PIPE

## Webcam source.
# @since		2015-08-13 18:48:51
class Raspistill ( Source ):

	## Where to find the binary.
	# @since		2018-04-03 11:46:05
	raspistill_bin = '/usr/bin/raspistill'

	def _init_( self, **kwargs ):
		self.id = 'raspistill'
		self.description = "Raspberry Camera"

	def capture_internal( self, **kwargs ):
		image = Image()
		image.filename = kwargs.get( 'filename' )

		# Execute fswebcam to fetch the image.
		p = Popen( [
			Raspistill.raspistill_bin,
			'--timeout',
			'1',
			'--nopreview',
			'-o',
			image.filename
		], stdout=PIPE, stderr=PIPE );
		err, out = p.communicate()

		return image

	## Initialize all webcam devices
	# @since		2015-08-16 14:54:48
	@staticmethod
	def init_all():
		# Is there a raspistill binary?
		if not os.path.exists( Raspistill.raspistill_bin ):
			return

		# Is there a raspistill connected?
		# Execute fswebcam to fetch the image.
		filename = app.get_temp_filename(
			dir = app.config[ 'DIRECTORIES' ][ 'IMAGES' ],
			prefix = 'raspiinit_{}_'.format( app.utc() ),
			suffix =  '.jpg'
		)
		p = Popen( [
			Raspistill.raspistill_bin,
			'--awb',
			'sun',
			'--exposure',
			'sports',
			'--timeout',
			'1',
			'-q',
			'75',
			'--nopreview',
			'-o',
			filename
		], stdout=PIPE, stderr=PIPE );
		err, out = p.communicate()

		if ( os.path.getsize( filename ) < 1 ):
			return

		# Delete the file to clean up.
		os.remove( filename )

		# File exists and is big.
		source = Raspistill()
		app.sources.append( source )
