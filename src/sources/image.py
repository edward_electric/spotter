import os

## A capture is an image on disk.
# @since		2015-08-13 20:14:37
class Image:
	def __init__( self ):
		## The path to the image on disk.
		# @since		2015-08-13 20:15:07
		self.filename = ''

		## The index of this image in the images object.
		# @since		2015-08-24 21:26:40
		self.index = 0

		## An optional description / annotation.
		# @since		2018-03-04 23:30:55
		self.description = ''

		## Keep this image until a source reset.
		# @details		Normally, images are purged after a few seconds.
		# @since		2016-10-30 20:36:09
		self.keeper = False

		## The next image object.
		# @since		2015-08-13 20:17:08
		self.next = False

		## The previous image object.
		# @since		2015-08-13 20:16:54
		self.previous = False

	## Our string.
	# @since		2015-08-24 21:41:42
	def __repr__( self ):
		return "{} {}".format( self.index, self.filename )

	## Delete the image from disk.
	# @since		2015-09-02 21:22:52
	def delete( self ):
		os.remove( self.filename )

	## Return the description
	# @since		2018-03-04 23:32:18
	def get_description( self ):
		return self.description

	## Keep this image permanently (or at least until a source reset).
	# @since		2015-08-27 21:28:03
	def keep( self ):
		self.keeper = True

	## Set the description.
	# @since		2018-03-04 23:31:50
	def set_description( self, description ):
		self.description = description

	## Set the filename.
	# @since		2015-08-13 20:16:26
	def set_filename( self, filename ):
		self.filename = filename
