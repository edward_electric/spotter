import glob
import os
import re
from image import Image
from src import app
from source import Source
from gevent.subprocess import Popen, PIPE

## Webcam source.
# @since		2015-08-13 18:48:51
class Webcam ( Source ):
	def _init_( self, **kwargs ):
		self.device = kwargs.get( 'device', '/dev/video0' )

		# Used together with skip to automatically figure out how much of a skip is necessary for this specific webcam.
		self.calibrated = False

		# Skip is to allow the camera to warm up. The minimum necessary for it to work seems to be 5.
		# This will self-increase until the image size is > 25kb.
		self.skip = 5

		# The ID we can base on the device.
		video_number = re.sub( '.*video', '', self.device )
		video_number = int( video_number )
		self.id = 'webcam{}'.format( video_number + 1)
		self.description = "Webcam {}".format( video_number + 1)

	def capture_internal( self, **kwargs ):
		image = Image()
		image.filename = kwargs.get( 'filename' )

		# Execute fswebcam to fetch the image.
		p = Popen( [
			app.config[ 'FSWEBCAM_PATH' ],
			'--no-banner',
			'--jpeg',
			'80',
			'-r',
			'100000x100000',			# Request a superhigh resolution that will automatically get downsized to the closest maximum.
			'-S',						# Skip...
			str( self.skip ),			# ... some frames to allow to autoexposure
			'-d',
			self.device,
			image.filename
		], stdout=PIPE, stderr=PIPE );
		err, out = p.communicate()

		# If the image is too small, then the camera isn't working properly and needs more skip to warm up.
		if not self.calibrated:
			if os.path.getsize( image.filename ) < 25000:
				self.skip = self.skip + 1
			else:
				# Give it two more frames for the sake of stabilization.
				self.skip = self.skip + 2
				self.calibrated = True
		else:
			# If calibrated, meaning it has been a while, and fswebcam dies, it's time for a restart of uvcvideo.
			if os.path.getsize( image.filename ) < 1 :
				p = Popen( [
					app.config[ 'RESET_SCRIPT' ]
				], stdout=PIPE, stderr=PIPE );
				err, out = p.communicate()

		return image

	## Initialize all webcam devices
	# @since		2015-08-16 14:54:48
	@staticmethod
	def init_all():
		# Find all files that are called /dev/video*
		for video in glob.glob( '/dev/video*' ):
			source = Webcam( device = video, description = 'Webcam' )
			app.sources.append( source )
