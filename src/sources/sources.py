from flask import abort, jsonify, redirect, render_template, request, send_file, url_for
from raspistill import Raspistill
from source import Source
from src import app
from threading import Thread
from webcam import Webcam
from zipfile import ZipFile

import time

## Source manager class
# @since		2015-08-12 22:21:57
class Sources( dict ):

	def init( self ):
		## A dict of sources.
		# @since		2015-08-13 19:11:22
		self.sources = {}
		Raspistill.init_all()
		Webcam.init_all()

	## Add a source.
	# @since		2015-08-13 19:07:00
	def append( self, source ):
		self[ source.id ] = source

	## Capture from all active sources.
	# @since		2015-08-21 13:33:38
	def capture( self ):
		for source_id, source in self.items():
			if source.is_active():
				source.capture()

	## Count the amount of sources.
	# @since		2015-08-12 23:20:57
	def count( self ):
		return len( self.sources )

	## Find a source using its ID.
	# @since		2016-10-30 20:00:15
	def find_source( self, source_id ):
		try:
			source = self[ source_id ]
		except:
			abort( 404 )
		return source

	## Initialize the threading.
	# @since		2016-11-06 19:48:49
	def init_threads( self ):
		for key in self.keys():
			if app.threads[ 'update_source' ].get( key, None ) is None:
				thread = Thread( target= self.update_source_thread, kwargs={ 'source_id' : key }  )
				app.threads[ 'update_source' ][ key ] = thread
				thread.start()

	## A source has a new image.
	# @since		2015-08-21 14:11:22
	def new_image_notification( self, source ):
		app.socketworker.new_image_notification( source )

	## Setup the routes.
	# @since		2015-08-21 13:30:04
	def setup_routes( self ):
		pass

		## Return a json list of sources.
		# @since		2016-11-08 18:09:52
		@app.route( '/api/sources/', methods = [ 'GET' ] )
		def route_sources_api_sources():
			r = {}
			r[ 'id' ] = app.get_id()
			r[ 'sources' ] = {}
			for source_id, source in self.items():
				r[ 'sources' ][ source_id ] = {
					'description' : source.get_description(),
					'preview_url' : url_for( 'route_sources_source_latest', source_id = source_id, random = app.utc(), _external = True ),
					'view_url' : url_for( 'route_sources_source_view_auto', source_id = source_id, _external = True ),
				}
			return jsonify( r )

		@app.route( '/source/<string:source_id>/settings/', methods = [ 'GET', 'PUT' ] )
		def route_sources_source_settings( source_id ):
			source = self.find_source( source_id )

			if request.method == 'GET':
				r = {
					'description' : source.description,
					'original_description' : source.original_description,
				}
				return jsonify( r )

			if request.method == 'PUT':
				json = request.get_json()

				description = json.get( 'description', None )
				if description is not None:
					description = description.encode("utf-8")
					description = app.normalize_input_string( description )
					# description = description.decode( 'string_escape' )
					if len( description ) < 1:
						description = source.original_description
					source.description = description

				# Set our time according to client time.
				time = json.get( 'time', None )
				if time is not None:
					import os
					os.system('sudo date -s "' + time + '"')

			return ''

		@app.route( '/source/<string:source_id>/download/' )
		def route_sources_source_download( source_id, random = '' ):
			source = self.find_source( source_id )

			from image import Image
			snapshots = source.images.get_keepers()

			# This is the file written to disk.
			zip_filename = "{}.zip".format( app.time() )
			zip_path = '{}/{}'.format( app.config[ 'DIRECTORIES' ][ 'TEMP' ], zip_filename )

			from gevent.subprocess import Popen, PIPE
			import os

			# Create a zip of all of the snapshots.
			with ZipFile( zip_path, 'w' ) as thezip:
				for snapshot_id in snapshots:
					image = source.images.get( snapshot_id )
					description = image.get_description()
					if description != "":
						image_filename = "{}.caption.jpg".format( image.filename )
						p = Popen( [
							app.imagemagick( "convert" ),
							'-background',
							"rgba(0,0,0,0.75)",
							'-fill',
							'white',
							'-pointsize',
							'25',
							'-size',
							"{}x40".format( source.image_dimensions[ 'width' ] ),
							'-gravity',
							'center',
							"caption:{}".format( description ),
							'-bordercolor',
							"rgba(0,0,0,0.75)",
							'border',
							"10",
							image.filename,
							'+swap',
							'-gravity',
							'south',
							'-geometry',
							'+0+0',
							'-composite',
							image_filename
						], stdout=PIPE, stderr=PIPE );
						out, err = p.communicate()
						output = out.strip()
					else:
						image_filename = image.filename
					nice_filename = '{} {}.jpg'.format( source.description, image.index )
					thezip.write( image_filename, arcname = nice_filename )
					if description != "":
						# Delete the caption file afterwards.
						os.remove( image_filename )

			# And send it to the user.
			download_zip_filename = '{} {} {}.zip'.format( source.description, app.time(), len( snapshots ) )
			return send_file(
				zip_path,
				attachment_filename = download_zip_filename,
				as_attachment = True,
				cache_timeout = 1
			)

		@app.route( '/source/<string:source_id>/latest/' )
		@app.route( '/source/<string:source_id>/latest/<string:random>' )
		def route_sources_source_latest( source_id, random = '' ):
			source = self.find_source( source_id )
			source.touch()
			source.capture()
			latest = source.images.get_latest_image()
			if not latest:
				abort( 404 )
			return send_file(
				latest.filename,
				attachment_filename = '{} {}.jpg'.format( source.id, source.images.get_latest_key() )
			)

		@app.route( '/source/<string:source_id>/image/<int:index>' )
		def route_sources_source_image( source_id, index ):
			source = self.find_source( source_id )
			source.touch()
			image = source.images.get( index )
			if image is None:
				abort( 404 )

			return send_file(
				image.filename,
				attachment_filename = '{} {}.jpg'.format( source.id, image )
			)

		@app.route( '/source/<string:source_id>/reset' )
		def route_sources_source_reset( source_id ):
			source = self.find_source( source_id )
			source.images.flush()
			return redirect( url_for( 'route_sources_source_view_auto', source_id = source.id ) )

		@app.route( '/source/<string:source_id>/ui/<string:ui_type>' )
		def route_sources_source_view( source_id, ui_type ):
			source = self.find_source( source_id )
			source.touch()

			# Check for a valid UI.
			if ui_type not in app.config[ 'UIS_AVAILABLE' ]:
				new_url = url_for( 'route_sources_source_view', source_id = source_id, ui_type = app.config[ 'UI_DEFAULT' ] )
				return redirect( new_url )

			return render_template( 'ui_{}.html'.format( ui_type ), source = source, ui_type = ui_type )

		## Used to allow JS to redirect the user to their UI type of choice.
		# @since		2016-10-30 16:33:01
		@app.route( '/source/<string:source_id>' )
		def route_sources_source_view_auto( source_id ):
			source = self.find_source( source_id )
			return render_template( 'ui_auto.html', source_id = source_id )

	## Return the sources sorted by name.
	# @since		2015-08-21 19:43:36
	def sorted( self ):
		# We need all of the descriptions.
		descriptions = []
		for source_id, source in self.items():
			descriptions.append( ( source.get_description(), source.id ) )
		items = []
		descriptions = sorted( descriptions )
		for ignore, source_id in descriptions:
			source = self[ source_id ]
			items.append( ( source_id, source ) )
		return items

	## Return a source using its ID.
	# @since		2015-08-19 20:43:57
	def source( self, source_id ):
		return self.get( source_id )

	## Update a source via a thread.
	# @since		2016-11-06 20:03:45
	def update_source_thread( self, **kwargs ):
		source_id = kwargs.get( 'source_id' )
		while True:
			if self.get( source_id ) is None:
				# Source does not exist anymore.
				del app.threads[ 'update_source' ][ source_id ]
				return
			self.source( source_id ).capture()
			time.sleep( 0.5 )
