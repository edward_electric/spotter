from image import Image
from src import app

## Container for a source's images.
# @since		2015-08-16 09:33:53
class Images( dict ):
	def __init__( self ):
		# A quick pointer to the latest image key.
		self.latest = False

	def append( self, image ):
		old_latest = self.get_latest_image()

		# And now add the image to our collection.
		utc = app.utc()
		image.index = utc
		self[ utc ] = image
		self.latest = utc

		if len( self ) is 1:
			image.next = image
			image.previous = image
		else:
			# After this latest image, comes the first image.
			# The first image is the old_latest's next.
			first = old_latest.next
			image.next = first
			image.previous = old_latest

			old_latest.next = image

			# And tell the first image about this new one at the end.
			first.previous = image

	## Delete an image and update all pointers.
	# @since		2016-09-16 19:19:51
	def delete( self, index ):
		image = self[ index ]
		previous = image.previous
		next = image.next

		# Reindex neighbors
		previous.next = next
		next.previous = previous

		# Tell the image to delete itself from disk.
		image.delete()
		# And remove it from our items.
		del self[ index ]

	## Flush out the images of this source.
	# @since		2015-09-02 21:21:33
	def flush( self ):
		for index, image in self.items():
			image.delete()
			del self[ index ]
		self.latest = False

	## Return an array of all image description.
	# @since		2018-03-04 23:45:09
	def get_descriptions( self ):
		r = dict()
		for index, image in self.items():
			description = image.get_description()
			if description != '':
				r[ index ] = image.get_description()
		return r

	## Return an array of all of the keepers.
	# @since		2016-10-30 20:39:58
	def get_keepers( self ):
		r = []
		for index, image in self.items():
			if image.keeper:
				r.append( index )
		r.sort()
		return r

	## Return the latest image captured.
	# @since		2015-08-16 12:43:12
	def get_latest_image( self ):
		if self.latest is False:
			return False
		return self[ self.latest ]

	## Return the index of the latest taken image.
	# @since		2015-08-24 20:48:30
	def get_latest_key( self ):
		return self.latest

	## Trim off old images. Keep count images in the store.
	# @since		2016-09-16 18:55:57
	def trim( self, max ):
		if len( self ) < 1:
			return;

		# Find the first image.
		latest = self.get_latest_image()
		current_image = latest.next

		# Increase the max to allow for the saved keepers.
		max = max + len( self.get_keepers() )

		while len( self ) > max:
			# Unable to find a non-keeper?
			if current_image == latest:
				break

			# Keepers do not max.
			if current_image.keeper:
				current_image = current_image.next
				continue

			self.delete( current_image.index )

			# Go to the next image.
			current_image = current_image.next
