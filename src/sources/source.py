from flask import url_for
from gevent.subprocess import Popen, PIPE
from image import Image
from src import app

import os

## Main source class.
# @since		2015-08-13 18:48:51
class Source:
	def __init__( self, **kwargs ):

		## Is this source busy capturing?
		# @since		2015-08-21 13:38:53
		self.busy = False

		## Cooldown time between captures
		# @since		2015-08-21 16:57:49
		self.cooldown = 0

		# A short description of this source.
		self.description = kwargs.get( 'description', 'No description' )
		self.original_description = self.description

		self.id = kwargs.get( 'id' )
		if self.id is None:
			import string
			import random
			id = 'source'
			for counter in range( 4 ):
				id = id + random.choice( string.ascii_uppercase + string.digits )
			self.id = id

		## The list of images for this source.
		# @since		2015-08-13 20:17:59
		from images import Images
		self.images = Images()

		## The extension of the captured images.
		# @since		2015-08-16 09:08:09
		self.image_type = 'jpg';

		## The width.height of the captured images.
		# @since		2015-08-16 09:08:31
		self.image_dimensions = { 'width' : 0, 'height' : 0 }

		## The parent source, if any.
		# @since		2015-08-13 19:33:13
		self.parent = kwargs.get( 'parent', False )

		## After timeout seconds the source is considered idle.
		# @since		2015-08-13 18:59:30
		self.timeout = 60

		## When this source was lasted touched.
		# @since		2015-08-13 19:00:20
		self.touched = 0

		# Allow subclasses to init themselves.
		self._init_( **kwargs )

	## How many images are in this source.
	# @since		2015-08-13 20:19:17
	def __len__( self ):
		return len( self.images )

	## Return a better description
	# @since		2015-08-16 12:39:23
	def __repr__( self ):
		return self.description

	## Internal init for subclasses.
	# @since		2015-08-16 09:02:51
	def _init_( self, **kwargs ):
		pass

	## Make a new capture.
	# @details		Returns false if nothing new was captured.
	# @since		2015-08-13 20:14:06
	def capture( self ):
		if self.is_busy():
			return self.images.get_latest_image()

		# Capture to this filename.
		filename = app.get_temp_filename(
			dir = app.config[ 'DIRECTORIES' ][ 'IMAGES' ],
			prefix = 'source_{}_{}_'.format( self.id, app.utc() ),
			suffix =  '.jpg'
		)

		if self.parent is False:
			# Mark ourselves as busy for a while.
			self.busy = app.utc() + self.timeout
			image = self.capture_internal( filename = filename )

			# Did the capture work well?
			if os.path.getsize( filename ) < 1024:
				os.remove( filename )
				self.no_longer_busy()
				return self.images.get_latest_image()

		else:
			self.parent.capture()
			# Return the latest image of the parent.
			image = Image()
			last_image = self.parent.images.get_latest_image()
			if last_image is False:
				return False

			# Have we already seen this filename?
			our_latest = self.images.get_latest_image()
			if our_latest is not False and our_latest.original_filename == last_image.filename:
				return False

			image.filename = filename
			image.original_filename = last_image.filename
			import shutil
			shutil.copy2( last_image.filename, image.filename )

		# We have to extract the image dimensions the first time.
		if not self.has_image_dimensions():
			self.extract_image_dimensions( image )

		# Add it to the source.
		self.images.append( image )

		# Trim off old images.
		self.images.trim( app.config[ 'KEEP_IMAGES' ] )

		self.no_longer_busy()

		app.sources.new_image_notification( self )

		return image

	## The internal capture method that does the actual capturing from the source.
	# @since		2015-08-13 20:21:42
	def capture_internal( self, **kwargs ):
		pass

	## Tell everyone about our keepers.
	# @since		2016-10-30 21:02:43
	def emit_keepers( self ):
		app.socketio.emit( 'get_keepers',
		{
			'source_id' : self.id,
			'descriptions' : self.images.get_descriptions(),
			'images' : self.images.get_keepers(),
		} )

	## Extract and set our image dimensions from this image.
	# @since		2015-08-16 09:48:58
	def extract_image_dimensions( self, image ):
		if self.parent is False:
			p = Popen( [
				app.imagemagick( "convert" ),
				image.filename,
				'-ping',
				'-format',
				'\'%[fx:w]x%[fx:h]\'',
				'info:'
			], stdout=PIPE, stderr=PIPE );
			out, err = p.communicate()
			output = out.strip()
			output = output.replace( "'", '' );
			import re
			self.image_dimensions[ 'width' ] = int( re.sub( r'x.*', '', output ) )
			self.image_dimensions[ 'height' ] = int( re.sub( r'.*x', '', output ) )
		else:
			# Child sources take the image dimensions from the parent source.
			self.image_dimensions = dict( self.parent.image_dimensions )

	## Allow the description to be overriden by child classes.
	# @since		2016-06-01 23:18:50
	def get_description( self ):
		return self.description

	## Convenience method to check whether we have any image dimensions.
	# @since		2015-08-16 13:18:25
	def has_image_dimensions( self ):
		return self.image_dimensions[ 'width' ] != 0

	## Keep an image.
	# @since		2016-10-31 22:15:07
	def keep_image( self, index ):
		image = self.images.get( index )
		image.keep()

		# Move the image to the keepers directory.
		basename = os.path.basename( image.filename )
		new_filename = app.config[ 'DIRECTORIES' ][ 'KEEPERS' ] + '/' + basename
		os.rename( image.filename, new_filename )
		image.set_filename( new_filename )

	## Is this source active? Has it been requested recently?
	# @since		2015-08-21 17:27:47
	def is_active( self ):
		return app.utc() - self.touched < self.timeout

	## Is this source busy capturing?
	# @since		2015-08-13 19:00:41
	def is_busy( self ):
		return self.busy >= app.utc()

	## Set outselves to no longer be busy/
	# @since		2015-09-17 23:23:55
	def no_longer_busy( self ):
		self.busy = app.utc() + self.cooldown

	## Return a string representation.
	# @since		2015-08-16 23:31:21
	def to_string( self ):
		return self.id

	## Touch this source, marking it as active.
	# @since		2015-08-13 18:58:33
	def touch( self ):
		self.touched = app.utc()

	def trash_image( self, index ):
		if index in self.images:
			del self.images[ index ]
