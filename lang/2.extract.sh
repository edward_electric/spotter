# Extract all strings into the messages.pot file.
# Next step: update.sh

source i18n.conf

rm $POT

cd ..

source venv.sh
pybabel extract -F $CFG -o $POT .
