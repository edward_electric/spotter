# Updates the pot of all the languages with the values found in messages.pot
# Next step: poedit.

source i18n.conf

# Up to root
cd ..

source venv.sh
pybabel compile -d src/translations
