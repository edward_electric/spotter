# Creates a new language in the translations directory.

LANG=$1
if [ "$LANG" = "" ]; then
	echo Syntax: init.sh LANG
	exit
fi

cd ..

POT="i18n/messages.pot"

cd ..
source venv.sh

cd app
pybabel init -i $POT -d translations -l $LANG
