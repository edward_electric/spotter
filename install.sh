#!/bin/bash

# This is the installation script for PTV.
# It will download all necessary packages and set up the Python environment in this directory.
# If you move the directory, you will have to rerun the installer.

# Yes, these are Debian packages, so you best not be running a BSD or Windows.

# Because git doesn't care about empty directories.
mkdir temp

# Refresh the package locations
sudo aptitude update

# Install necessary packages
sudo aptitude install -r python-pip python-virtualenv fswebcam libpython-dev gunicorn imagemagick

# Copy the sudoer file to allow the machine to be shut down.
sudo cp install/plaintargetview /etc/sudoers.d/

rm -rf venv
virtualenv venv
. venv/bin/activate venv
pip install flask flask-socketio Flask-Babel Flask-Session gevent
