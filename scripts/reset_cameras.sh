#!/bin/bash

# Some cameras, like the Logitech B525, sometimes stop working after a while. For no reason.
# This script is run automatically by PTV when it detects an image file size of 0.

sudo rmmod uvcvideo
sleep 1
sudo modprobe uvcvideo
sleep 1
