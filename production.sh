#!/bin/bash
. venv/bin/activate venv
while ( true ) ; do
	clear
	date
	# Delete the pyc files
	find src -iname *.pyc -exec rm -v {} \;
	python production.py
	sleep 0.5
done
